$(document).ready(function(){
  slidenb=1;
  $('#end').hide();



  $("body").keydown(function(e) {
    if(e.keyCode == 37) { // left
      slidenb-=1;
      if (slidenb<=0){slidenb=28}

      $('#ailedroite p').hide();
      $('#ailegauche img').hide();


      checkleg();
      checkchap();


      $('#ailedroite p:nth-child('+slidenb+')').fadeIn();
      $('#ailegauche img:nth-child('+slidenb+')').fadeIn();
    }
    else if(e.keyCode == 39) { // right
      slidenb+=1;
      if (slidenb>28){slidenb=1}

      $('#ailedroite p').hide();
      $('#ailegauche img').hide();

      checkleg();
      checkchap();

      $('#ailedroite p:nth-child('+slidenb+')').fadeIn();
      $('#ailegauche img:nth-child('+slidenb+')').fadeIn();
    }
  });

function checkleg(){
  if (slidenb!=1 && slidenb!=2 && slidenb!=28 && slidenb!=27){
    var leg=$('#ailegauche img:nth-child('+slidenb+')').attr('alt');
    $("#legendes").html("↤ "+leg);
  }else{
    $("#legendes").html("");
  }
}

function checkchap(){
  var chap=$('#ailegauche img:nth-child('+slidenb+')').attr('chap');
  var achap=$("#chap").html(chap);
  if (chap && chap!=achap){
    $("#chap").html(chap);
  }
}


  $('#ailedroite p').hide();
  $('#ailegauche img').hide();
  $('#chap span').hide();

  $('#ailedroite p:nth-child('+slidenb+')').fadeIn();
  $('#ailegauche img:nth-child('+slidenb+')').fadeIn();
  $('#chap span:nth-child('+slidenb+')').fadeIn();
});
